# WordPress Plugin - DPG Agent Exporter
This plugin will export a single agent's data from a real estate agency's main site into a single agent site.

## Setup
### Instructions for composer.json
* Configure package repository.
* Configure WP and plugin directories.

Example composer.json:
````
{
  "repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/dpgteam/wp-dpg-agent-exporter.git"
    }
  ],
  "require": {
    "dpgteam/wp-dpg-agent-exporter": "*",
  },
  "extra": {
    "wordpress-install-dir": "public/wp",
    "installer-paths": {
      "public/app/plugins/{$name}/": [
        "type:wordpress-plugin"
      ],
    }
  }
}
````
### WP Admin Instructions
Activate the plugin via the WordPress Admin menu.
#### Required Plugins
The following plugins need to be installed and activated:

  * Advanced Custom Fields PRO

  * JSON API

#### WP JSON API settings
Activate the **DPG Agent Exporter** controller.

* In the admin menu select *Settings > JSON API*.

* Locate the **DPG Agent Exporter** in the list and click *Activate*.





