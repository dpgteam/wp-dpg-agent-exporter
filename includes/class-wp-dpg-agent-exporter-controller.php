<?php
/**
Controller name: DPG Agent Exporter
Controller description: Exports property data for a single agent.
 */

/**
 * DPG Agent Export Controller
 *
 * @link       http://digitalpropertygroup.com
 * @since      1.0.0
 *
 * @package    Wp_DPG_Agent_Exporter
 * @subpackage Wp_DPG_Agent_Exporter/includes
 */

/**
 * DPG Agent Export Controller
 *
 * This class defines endpoints for getting an agent's property data.
 *
 * @since      1.0.0
 * @package    Wp_DPG_Agent_Exporter
 * @subpackage Wp_DPG_Agent_Exporter/includes
 * @author     Paul Beynon <paul@digitalpropertygroup.com>
 */
class JSON_API_Dpg_Agent_Export_Controller {
	/* How many properties to get per request. */
	protected $posts_per_page = 10;
	/* Parameters passed with GET request. */
	protected $params = [];
	/* An array of agent post data. */
	protected $agent = [];
	/* An array of this agent's property data. */
	protected $properties = [];
	/**
	 * Stores query parameters to class.
	 */
	public function __construct() {
		$this->params();
	}
 	public static function wp_dpg_importer_allow_cors() {
         header( "Access-Control-Allow-Origin: *" );
    }
	/**
	 * Parses request query string to array.
	 * @return array
	 */
	protected function params() {
		if ( ! $this->params ) {
			parse_str($_SERVER['QUERY_STRING'], $params);
			$this->params = $params;
			$this->params['offset'] = $this->params['offset'] ?? 0;
			$this->params['properties'] = $this->params['properties'] ?? true;
			$this->params['properties'] = $this->params['properties'] === 'false'? false: true;
		}
		return $this->params;
	}

	public function export() {
		try {
			if ( $this->params['agent_slug']) {
				/* Set the agent for this query. */
				$this->set_agent_data();
				
				/* Get the agent's properties. */
				if ( $this->params['properties'] !== false) {
					$this->set_agent_properties();
				} 
				$posts = get_field('office_salesagent', $this->agent['ID']);
				$data = $this->agent;
				$office_fields = [ 
					'office_propertymanager',
					'office_salesagent',
					'office_admin',
					'office_principal',
				];
				$data['offices'] = [];
				/* Get unique list of offices this agent is attached to. */
				foreach( $office_fields as $field ) {
					$field_data = get_field($field, $this->agent['ID']);
					if ( $field_data ) {
						$data['offices'] = array_merge(get_field($field, $this->agent['ID']));
					}
				}
				/* Add featured office image. */
				foreach( $data['offices'] as $key => $office ) {
					$data['offices'][$key]->location = get_field('google_map', $office->ID);
					$data['offices'][$key]->image = $this->makeFeaturedImageArray($office->ID);
				}
				$data['properties'] = $this->properties;
				$result = ['result' => $data];
			} else {
				$result = ['result' => 'No agent selected'];
			}
			return $result;
		} catch (Exception $e) {
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
		}
	}
	protected function set_agent_data() {
		if ( $this->params['agent_slug'] && ! count($this->agent) ) :
			$args = [
			  'name'        => $this->params['agent_slug'],
			  'post_type'   => 'agent',
			  'post_status' => 'publish',
			  'numberposts' => 1
			];
			$this->agent = get_posts($args);
			if ( count($this->agent) ) {
				$this->agent = get_object_vars($this->agent[0]);
				$this->agent['fields'] = get_fields($this->agent['ID']);
				$this->agent['fields']['featured_image'] = $this->makeFeaturedImageArray($this->agent['ID']);
				unset($this->agent['fields']['property_agent']);
				unset($this->agent['fields']['office_salesagent']);
				unset($this->agent['fields']['office_admin']);
				unset($this->agent['fields']['office_principal']);
				unset($this->agent['fields']['office_propertymanager']);
			}
 		endif;
		return count($this->agent) > 0 ? true : false;
	}

	protected function set_agent_properties() {
		if ( count($this->agent) && ! count($this->properties) ) :
			if ( $this->params['properties'] !== false ) {
				$args = [
				    'post_type'      => 'property',
				    'posts_per_page' => $this->posts_per_page,
				    'offset'  		 => $this->params['offset'] * $this->posts_per_page,
	 				'orderby' 		 => 'publish_date', 
	 				'order'   		 => 'DESC',
				    'meta_query' => array(
				        [
				            'key'     => 'property_agent',
				            'value'   => '"' . $this->agent['ID'] . '"',
				            'compare' => 'LIKE'
				        ],
				    )
				];
				$this->properties = get_posts($args);
				foreach ( $this->properties as $key => $property) :
					$fields = get_fields($property->ID);
					$this->properties[$key] = $object = get_object_vars($this->properties[$key]);
					$this->properties[$key]['fields'] = get_fields($this->properties[$key]['ID']);
				endforeach;
			}
		endif;
		return count($this->properties) > 0 ? true : false;
	}
	/**
	 * Accepts an array of neighbourhood post names, gets their 
	 * post content, custom fields and featured image sizes.
	 * @return array 
	 */
	public function export_neighbourhoods() {
		try {
			$suburbs = $this->params['neighbourhoods'] ?? []; // eg. ['oak-park', 'gisborne', 'pascoe-vale']
			$query = new WP_Query([
				'post_name__in' => $suburbs,
				'post_type'     => 'neighbourhood',
				'post_status'   => ['draft', 'pending', 'future', 'publish', 'private'],
				'numberposts'   => 1
			]);
			$neighbourhoods = $query->posts;
			foreach( $neighbourhoods as $key => $neighbourhood) {
				$neighbourhoods[$key]->fields = get_fields($neighbourhood->ID);
				$neighbourhoods[$key]->fields['featured_image'] = $this->makeFeaturedImageArray($neighbourhood->ID);
			}
			return [
				'neighbourhoods' => $neighbourhoods,
			];
		} catch (Exception $e) {
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
		}
	}
	/**
	 * Makes a list of $post thumbnail image sizes with image
	 * dimensions and full URL to image file.
	 * @param  integer $post
	 * @return array         
	 */
	protected function makeFeaturedImageArray($post_id) {
		$id   = get_post_thumbnail_id($post_id);
		$meta = wp_get_attachment_metadata($id);
		$images = [];
		$images['full'] = [
			'size'	 => 'full',
			'width'  => $meta['width'],
			'height' => $meta['height'],
			'url' 	 => get_the_post_thumbnail_url($post_id),
		];
		foreach( $meta['sizes'] as $key => $size ) {
			$images[$key] = [
				'size'	 => $key,
				'width'  => $size['width'],
				'height' => $size['height'],
				'url' 	 => get_the_post_thumbnail_url($post_id, $key),
			];
		}
		return $images;
	}
}
