<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://digitalpropertygroup.com
 * @since      1.0.0
 *
 * @package    Wp_DPG_Agent_Exporter
 * @subpackage Wp_DPG_Agent_Exporter/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Wp_DPG_Agent_Exporter
 * @subpackage Wp_DPG_Agent_Exporter/includes
 * @author     Paul Beynon <paul@digitalpropertygroup.com>
 */
class Wp_DPG_Agent_Exporter {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Wp_DPG_Agent_Exporter_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $wp_dpg_agent_exporter    The string used to uniquely identify this plugin.
	 */
	protected $wp_dpg_agent_exporter;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->wp_dpg_agent_exporter = 'wp-dpg-agent-exporter';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		// Setup Endpoint
		$this->loader->add_filter('json_api_controllers', 'Wp_DPG_Agent_Exporter', 'add_dpg_agent_export_endpoint', 10, 1);
		$this->loader->add_filter('json_api_dpg_agent_export_controller_path', 'Wp_DPG_Agent_Exporter', 'set_dpg_agent_export_controller_path', 10, 1);
		$this->loader->add_action('json_api', 'JSON_API_Dpg_Agent_Export_Controller', 'wp_dpg_importer_allow_cors', 10, 2);
	}
	
	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Wp_DPG_Agent_Exporter_Loader. Orchestrates the hooks of the plugin.
	 * - Wp_DPG_Agent_Exporter_i18n. Defines internationalization functionality.
	 * - Wp_DPG_Agent_Exporter_Admin. Defines all hooks for the admin area.
	 * - Wp_DPG_Agent_Exporter_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wp-dpg-agent-exporter-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wp-dpg-agent-exporter-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wp-dpg-agent-exporter-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-wp-dpg-agent-exporter-public.php';
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wp-dpg-agent-exporter-controller.php';

		$this->loader = new Wp_DPG_Agent_Exporter_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Wp_DPG_Agent_Exporter_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Wp_DPG_Agent_Exporter_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Wp_DPG_Agent_Exporter_Admin( $this->get_wp_dpg_agent_exporter(), $this->get_version() );

		// $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		// $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Wp_DPG_Agent_Exporter_Public( $this->get_wp_dpg_agent_exporter(), $this->get_version() );

		// $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		// $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_wp_dpg_agent_exporter() {
		return $this->wp_dpg_agent_exporter;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Wp_DPG_Agent_Exporter_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	public static function add_dpg_agent_export_endpoint($controllers) {
	    $controllers[] = 'dpg_agent_export';
	    return $controllers;
	}
	public static function set_dpg_agent_export_controller_path()
	{
    	return __DIR__ . '/class-wp-dpg-agent-exporter-controller.php';
	}
	
}
