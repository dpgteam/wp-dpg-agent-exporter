<?php

/**
 * Fired during plugin activation
 *
 * @link       http://digitalpropertygroup.com
 * @since      1.0.0
 *
 * @package    Wp_DPG_Agent_Exporter
 * @subpackage Wp_DPG_Agent_Exporter/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_DPG_Agent_Exporter
 * @subpackage Wp_DPG_Agent_Exporter/includes
 * @author     Paul Beynon <paul@digitalpropertygroup.com>
 */
class Wp_DPG_Agent_Exporter_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
