<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://digitalpropertygroup.com
 * @since             0.0.1
 * @package           Wp_DPG_Agent_Exporter
 *
 * @wordpress-plugin
 * Plugin Name:       DPG Agent Exporter
 * Plugin URI:        http://digitalpropertygroup.com/
 * Description:       Exports a single agent's data so that it can be imported to another site.
 * Version:           0.0.20
 * Author:            Digital Property Group
 * Author URI:        http://digitalpropertygroup.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-dpg-agent-exporter
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wp-dpg-agent-exporter-activator.php
 */
function activate_wp_dpg_agent_exporter() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-dpg-agent-exporter-activator.php';
	Wp_DPG_Agent_Exporter_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wp-dpg-agent-exporter-deactivator.php
 */
function deactivate_wp_dpg_agent_exporter() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-dpg-agent-exporter-deactivator.php';
	Wp_DPG_Agent_Exporter_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wp_dpg_agent_exporter' );
register_deactivation_hook( __FILE__, 'deactivate_wp_dpg_agent_exporter' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wp-dpg-agent-exporter.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wp_dpg_agent_exporter() {

	$plugin = new Wp_DPG_Agent_Exporter();
	$plugin->run();

}
run_wp_dpg_agent_exporter();
